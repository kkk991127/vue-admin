import  {useAuthStore} from "~/stores/auth";

export default defineNuxtRouteMiddleware((to) => {
    const { authenticated } = storeToRefs(useAuthStore()); // make authenticated state reactive
    const token = useCookie('token'); // get token from cookies
    // 쿠키에 토큰이 있으면 값을 true로 바뀌어
    if (token.value) {
        // check if value exists
        authenticated.value = true; // update the state to authenticated
    }

    // if token exists and url is /login redirect to homepage
    if (token.value && to?.name === 'login') {
        return navigateTo('/');
    }
    // ? - 있는데 to는 현재 페이지 이름이 뭐냐
    // index로 보낸다

    // if token doesn't exist redirect to log in
    if (!token.value && to?.name !== 'login') {
        abortNavigation();
        return navigateTo('/login');
    }
    // 토큰이 없는데 시도중 -- 로그인 페이지로 보내줘야 함
});